var content='<div class="ui-page" deviceName="iPhoneX" deviceType="mobile" deviceWidth="375" deviceHeight="812">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devMobile devIOS canvas firer commentable non-processed" alignment="left" name="Template 1" width="375" height="812">\
    <div id="backgroundBox"></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1587356274199.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1587356274199-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1587356274199-ie8.css" /><![endif]-->\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-c716c12d-69f0-4863-8c15-f96c00faf0ff" class="screen growth-vertical devMobile devIOS canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Login" width="375" height="812">\
    <div id="backgroundBox"></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/c716c12d-69f0-4863-8c15-f96c00faf0ff-1587356274199.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/screens/c716c12d-69f0-4863-8c15-f96c00faf0ff-1587356274199-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="./resources/screens/c716c12d-69f0-4863-8c15-f96c00faf0ff-1587356274199-ie8.css" /><![endif]-->\
      <div id="s-sign-up-2" class="group firer ie-background commentable non-processed" datasizewidth="386px" datasizeheight="771px" dataX="-2" dataY="0" >\
        <div id="s-Rectangle_2" class="pie rectangle firer commentable non-processed"   datasizewidth="386px" datasizeheight="771px" dataX="0" dataY="0" >\
         <div class="backgroundLayer"></div>\
         <div class="paddingLayer">\
           <div class="clipping">\
             <div class="content">\
               <div class="valign">\
                 <span id="rtr-s-Rectangle_2_0"></span>\
               </div>\
             </div>\
           </div>\
         </div>\
        </div>\
        <div id="s-Text_2" class="pie label singleline autofit firer ie-background commentable non-processed"   datasizewidth="142px" datasizeheight="59px" dataX="128" dataY="63" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Text_2_0">Sign up</span></div></div></div></div>\
        <div id="s-Input_5" class="pie text firer commentable non-processed"  datasizewidth="306px" datasizeheight="45px" dataX="45" dataY="165" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="Username"/></div></div>  </div></div>\
        <div id="s-Input_6" class="pie text firer commentable non-processed"  datasizewidth="306px" datasizeheight="45px" dataX="45" dataY="244" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="Email"/></div></div>  </div></div>\
        <div id="s-Input_7" class="pie password firer commentable non-processed"  datasizewidth="306px" datasizeheight="45px" dataX="45" dataY="329" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><input type="password"  value="" maxlength="100"  tabindex="-1"  placeholder="Password"/></div></div></div></div>\
        <div id="s-Input_8" class="pie password firer commentable non-processed"  datasizewidth="306px" datasizeheight="45px" dataX="46" dataY="409" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><input type="password"  value="" maxlength="100"  tabindex="-1"  placeholder="Confirm Password"/></div></div></div></div>\
        <div id="s-Button_2" class="pie button singleline firer click commentable non-processed"   datasizewidth="306px" datasizeheight="45px" dataX="45" dataY="496" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Button_2_0">SUBMIT</span></div></div></div></div>\
        <div id="s-Paragraph_2" class="pie richtext firer ie-background commentable non-processed"   datasizewidth="219px" datasizeheight="18px" dataX="115" dataY="574" >\
          <div class="backgroundLayer"></div>\
          <div class="paddingLayer">\
            <div class="clipping">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_2_0">I agree with terms of use and privacy</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Checkbox_2" class="group firer ie-background commentable non-processed" datasizewidth="22px" datasizeheight="22px" dataX="82" dataY="571" >\
          <div id="s-Rectangle_8" class="pie rectangle firer ie-background commentable non-processed"   datasizewidth="22px" datasizeheight="22px" dataX="0" dataY="0" >\
           <div class="backgroundLayer"></div>\
           <div class="paddingLayer">\
             <div class="clipping">\
               <div class="content">\
                 <div class="valign">\
                   <span id="rtr-s-Rectangle_8_0"></span>\
                 </div>\
               </div>\
             </div>\
           </div>\
          </div>\
          <div id="s-Image_3" class="pie image lockV firer ie-background commentable non-processed"   datasizewidth="11px" datasizeheight="9px" dataX="6" dataY="7" aspectRatio="0.8181818"   alt="image" systemName="./images/5a86bef8-9b81-4d64-8cc2-add7b9c6d8e7.svg" overlay="#FFFFFF">\
              <?xml version="1.0" encoding="UTF-8"?>\
              <svg preserveAspectRatio=\'none\' width="15px" height="12px" viewBox="0 0 15 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
                  <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
                  <title>check</title>\
                  <desc>Created with Sketch.</desc>\
                  <defs></defs>\
                  <g id="s-Image_3-Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                      <g id="s-Image_3-Components" transform="translate(-376.000000, -645.000000)" fill="#A7C15B">\
                          <g id="s-Image_3-check" transform="translate(376.000000, 645.000000)">\
                              <path d="M0.173076923,6.6 C0.0576923077,6.48 0,6.3 0,6.18 C0,6.06 0.0576923077,5.88 0.173076923,5.76 L0.980769231,4.92 C1.21153846,4.68 1.55769231,4.68 1.78846154,4.92 L1.84615385,4.98 L5.01923077,8.52 C5.13461538,8.64 5.30769231,8.64 5.42307692,8.52 L13.1538462,0.18 L13.2121154,0.18 C13.4423077,-0.06 13.7890385,-0.06 14.0192308,0.18 L14.8269231,1.02 C15.0576923,1.26 15.0576923,1.62 14.8269231,1.86 L5.59615385,11.82 C5.48076923,11.94 5.36538462,12 5.19230769,12 C5.01923077,12 4.90384615,11.94 4.78846154,11.82 L0.288461538,6.78 L0.173076923,6.6 Z" id="s-Image_3-Fill-1"></path>\
                          </g>\
                      </g>\
                  </g>\
              </svg>\
          </div>\
          <div id="s-Hotspot_2" class="imagemap firer toggle ie-background commentable non-processed"   datasizewidth="22px" datasizeheight="22px" dataX="0" dataY="0"  >\
              <div class="clickableSpot"></div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Image_1" class="pie image firer ie-background commentable non-processed"   datasizewidth="305px" datasizeheight="183px" dataX="44" dataY="597"   alt="image">\
          <img src="./images/51136fac-2306-4e98-bf29-25ed932a18e8.jpg" />\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;