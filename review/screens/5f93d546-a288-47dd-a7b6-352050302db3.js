var content='<div class="ui-page" deviceName="iPhoneX" deviceType="mobile" deviceWidth="375" deviceHeight="812">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devMobile devIOS canvas firer commentable non-processed" alignment="left" name="Template 1" width="375" height="812">\
    <div id="backgroundBox"></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1587356274199.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1587356274199-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1587356274199-ie8.css" /><![endif]-->\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-5f93d546-a288-47dd-a7b6-352050302db3" class="screen growth-vertical devMobile devIOS canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Activities" width="375" height="812">\
    <div id="backgroundBox"></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/5f93d546-a288-47dd-a7b6-352050302db3-1587356274199.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/screens/5f93d546-a288-47dd-a7b6-352050302db3-1587356274199-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="./resources/screens/5f93d546-a288-47dd-a7b6-352050302db3-1587356274199-ie8.css" /><![endif]-->\
      <div id="s-Empty_Screen_Dark" class="group firer ie-background commentable non-processed" datasizewidth="375px" datasizeheight="812px" dataX="0" dataY="0" >\
        <div id="s-Rectangle_3" class="pie percentage rectangle firer commentable pin vpin-beginning hpin-beginning non-processed-percentage non-processed-pin non-processed"   datasizewidth="100%" datasizeheight="100%" dataX="0" dataY="0" >\
         <div class="backgroundLayer"></div>\
         <div class="paddingLayer">\
           <div class="clipping">\
             <div class="content">\
               <div class="valign">\
                 <span id="rtr-s-Rectangle_3_0"></span>\
               </div>\
             </div>\
           </div>\
         </div>\
        </div>\
        <div id="s-Rectangle_7" class="pie percentage rectangle firer commentable pin vpin-beginning hpin-beginning non-processed-percentage non-processed-pin non-processed"   datasizewidth="100%" datasizeheight="141px" dataX="0" dataY="0" >\
         <div class="backgroundLayer"></div>\
         <div class="paddingLayer">\
           <div class="clipping">\
             <div class="content">\
               <div class="valign">\
                 <span id="rtr-s-Rectangle_7_0"></span>\
               </div>\
             </div>\
           </div>\
         </div>\
        </div>\
        <div id="s-Paragraph_8" class="pie richtext firer ie-background commentable pin vpin-beginning hpin-beginning non-processed-pin non-processed"   datasizewidth="343px" datasizeheight="50px" dataX="15" dataY="86" >\
          <div class="backgroundLayer"></div>\
          <div class="paddingLayer">\
            <div class="clipping">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_8_0">Activities</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_6" class="pie percentage rectangle firer commentable pin vpin-end hpin-beginning non-processed-percentage non-processed-pin non-processed"   datasizewidth="100%" datasizeheight="84px" dataX="0" dataY="0" >\
         <div class="backgroundLayer"></div>\
         <div class="paddingLayer">\
           <div class="clipping">\
             <div class="content">\
               <div class="valign">\
                 <span id="rtr-s-Rectangle_6_0"></span>\
               </div>\
             </div>\
           </div>\
         </div>\
        </div>\
        <div id="s-Home_Indicator" class="pie image firer ie-background commentable pin vpin-end hpin-center non-processed-pin non-processed"   datasizewidth="134px" datasizeheight="5px" dataX="0" dataY="12"   alt="image" systemName="./images/894248bb-cd65-4469-9d6f-8bd9cc059235.svg" overlay="#FFFFFF">\
            <?xml version="1.0" encoding="UTF-8"?>\
            <svg preserveAspectRatio=\'none\' width="134px" height="5px" viewBox="0 0 134 5" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
                <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->\
                <title>Rectangle</title>\
                <desc>Created with Sketch.</desc>\
                <defs></defs>\
                <g id="UI-Bars-/-Home-Indicator-/-Home-Indicator---On-Light" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-121.000000, -20.000000)">\
                    <rect id="s-Home_Indicator-Rectangle" fill="#000000" x="121" y="20" width="134" height="5" rx="2.5"></rect>\
                </g>\
            </svg>\
        </div>\
        <div id="s-Text_2" class="pie label singleline firer pageload ie-background commentable pin vpin-beginning hpin-beginning non-processed-pin non-processed"   datasizewidth="45px" datasizeheight="19px" dataX="30" dataY="12" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Text_2_0">4:02</span></div></div></div></div>\
        <div id="s-Image_17" class="pie image firer ie-background commentable pin vpin-beginning hpin-end non-processed-pin non-processed"   datasizewidth="17px" datasizeheight="12px" dataX="63" dataY="12"   alt="image" systemName="./images/bd3ac1e6-b9fd-4ac7-b8b2-0f9ffe0813a9.svg" overlay="#FFFFFF">\
            <?xml version="1.0" encoding="UTF-8"?>\
            <svg preserveAspectRatio=\'none\' width="18px" height="12px" viewBox="0 0 18 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
                <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->\
                <title>Mobile Signal</title>\
                <desc>Created with Sketch.</desc>\
                <defs></defs>\
                <g id="UI-Bars-/-Status-Bars-/-Black-Base" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-293.000000, -17.000000)">\
                    <path d="M294.666667,24.3333333 L295.666667,24.3333333 C296.218951,24.3333333 296.666667,24.7810486 296.666667,25.3333333 L296.666667,27.3333333 C296.666667,27.8856181 296.218951,28.3333333 295.666667,28.3333333 L294.666667,28.3333333 C294.114382,28.3333333 293.666667,27.8856181 293.666667,27.3333333 L293.666667,25.3333333 C293.666667,24.7810486 294.114382,24.3333333 294.666667,24.3333333 Z M299.333333,22.3333333 L300.333333,22.3333333 C300.885618,22.3333333 301.333333,22.7810486 301.333333,23.3333333 L301.333333,27.3333333 C301.333333,27.8856181 300.885618,28.3333333 300.333333,28.3333333 L299.333333,28.3333333 C298.781049,28.3333333 298.333333,27.8856181 298.333333,27.3333333 L298.333333,23.3333333 C298.333333,22.7810486 298.781049,22.3333333 299.333333,22.3333333 Z M304,20 L305,20 C305.552285,20 306,20.4477153 306,21 L306,27.3333333 C306,27.8856181 305.552285,28.3333333 305,28.3333333 L304,28.3333333 C303.447715,28.3333333 303,27.8856181 303,27.3333333 L303,21 C303,20.4477153 303.447715,20 304,20 Z M308.666667,17.6666667 L309.666667,17.6666667 C310.218951,17.6666667 310.666667,18.1143819 310.666667,18.6666667 L310.666667,27.3333333 C310.666667,27.8856181 310.218951,28.3333333 309.666667,28.3333333 L308.666667,28.3333333 C308.114382,28.3333333 307.666667,27.8856181 307.666667,27.3333333 L307.666667,18.6666667 C307.666667,18.1143819 308.114382,17.6666667 308.666667,17.6666667 Z" id="s-Image_17-Mobile-Signal" fill="#000000" fill-rule="nonzero"></path>\
                </g>\
            </svg>\
        </div>\
        <div id="s-Image_18" class="pie image firer ie-background commentable pin vpin-beginning hpin-end non-processed-pin non-processed"   datasizewidth="18px" datasizeheight="18px" dataX="40" dataY="9"   alt="image" systemName="./images/10f95b4f-cddd-4913-b8c9-15cd1cc2bf96.svg" overlay="#FFFFFF">\
            <svg preserveAspectRatio=\'none\' id="s-Image_18-Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>cacaca</title><path d="M12,8.06a12.51,12.51,0,0,1,8.27,3.12L21.8,9.46A15,15,0,0,0,12,5.54,15,15,0,0,0,2.2,9.45l1.53,1.72A12.49,12.49,0,0,1,12,8.06"/><path d="M12,13a7.6,7.6,0,0,1,5,1.85l1.63-1.82A10.07,10.07,0,0,0,12,10.5,10.08,10.08,0,0,0,5.4,13L7,14.87A7.61,7.61,0,0,1,12,13"/><path d="M15.34,16.69A5.24,5.24,0,0,0,12,15.4a5.24,5.24,0,0,0-3.34,1.29L12,20.44Z"/></svg>\
        </div>\
        <div id="s-Image_4" class="pie image firer ie-background commentable pin vpin-beginning hpin-end non-processed-pin non-processed"   datasizewidth="25px" datasizeheight="12px" dataX="10" dataY="14"   alt="image" systemName="./images/f0f83702-1010-4874-bb90-b317aa47b7a6.svg" overlay="#FFFFFF">\
            <?xml version="1.0" encoding="UTF-8"?>\
            <svg preserveAspectRatio=\'none\' width="25px" height="12px" viewBox="0 0 25 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
                <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->\
                <title>Battery</title>\
                <desc>Created with Sketch.</desc>\
                <defs></defs>\
                <g id="Bars/Status-Bar/Dark-Status-Bar" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-336.000000, -17.000000)">\
                    <g id="s-Image_4-Battery" transform="translate(336.000000, 17.000000)">\
                        <rect id="s-Image_4-Border" stroke="#FFFFFF" opacity="0.35" x="0.5" y="0.833333333" width="21" height="10.3333333" rx="2.66666675"></rect>\
                        <path d="M23,4 L23,8 C23.8047311,7.66122348 24.328038,6.87313328 24.328038,6 C24.328038,5.12686672 23.8047311,4.33877652 23,4" id="s-Image_4-Cap" fill="#FFFFFF" fill-rule="nonzero" opacity="0.4"></path>\
                        <rect id="s-Image_4-Capacity" fill="#FFFFFF" fill-rule="nonzero" x="2" y="2.33333333" width="18" height="7.33333333" rx="1.33333337"></rect>\
                    </g>\
                </g>\
            </svg>\
        </div>\
        <div id="s-Image_11" class="pie image lockV firer click ie-background commentable non-processed"   datasizewidth="28px" datasizeheight="28px" dataX="28" dataY="47" aspectRatio="1.0"   alt="image" systemName="./images/6cf4c650-d9ec-49dc-b17b-e3b80de9d00b.svg" overlay="#FFFFFF">\
            <svg preserveAspectRatio=\'none\' id="s-Image_11-Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64"><title>1</title><rect x="8.5" y="29.65" width="47" height="4.7"/><rect x="8.5" y="15.55" width="47" height="4.7"/><rect x="8.5" y="43.75" width="47" height="4.7"/></svg>\
        </div>\
        <div id="s-Image_12" class="pie image lockV firer ie-background commentable non-processed"   datasizewidth="24px" datasizeheight="28px" dataX="286" dataY="47" aspectRatio="1.1666666"   alt="image" systemName="./images/2db798ff-4385-4ff1-983f-641cff37e5ba.svg" overlay="#FFFFFF">\
            <svg preserveAspectRatio=\'none\' id="s-Image_12-Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64"><defs><style>#s-Image_12 .cls-1{fill:#666;}</style></defs><title>alarm</title><path class="cls-1" d="M55,52a1,1,0,1,0,0-2h0a5,5,0,0,1-5-5.08l-1-13c-0.88-12.35-6.69-20.64-15-21.78V7a1,1,0,0,0-1-1H31a1,1,0,0,0-1,1v3.15c-8.31,1.14-14.12,9.43-15,21.77L14,45a5,5,0,0,1-5,5,1,1,0,0,0,0,2H55ZM13.9,50A6.84,6.84,0,0,0,16,45.08l1-13C17.87,19.88,23.76,12,32,12s14.13,7.88,15,20.08L48,45a7,7,0,0,0,2.11,5H13.9Z"/><path class="cls-1" d="M36.2,54.29a6,6,0,0,1-8.4,0,1,1,0,1,0-1.4,1.42,8,8,0,0,0,11.2,0A1,1,0,0,0,36.2,54.29Z"/></svg>\
        </div>\
        <div id="s-Image_28" class="pie image lockV firer ie-background commentable non-processed"   datasizewidth="21px" datasizeheight="21px" dataX="322" dataY="51" aspectRatio="1.0"   alt="image" systemName="./images/a14b26a6-f780-4a31-ba07-51b519c70e92.svg" overlay="#FFFFFF">\
            <svg preserveAspectRatio=\'none\' version="1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M23.854 23.146l-9.026-9.026c1.335-1.502 2.172-3.457 2.172-5.62 0-4.687-3.813-8.5-8.5-8.5s-8.5 3.813-8.5 8.5 3.813 8.5 8.5 8.5c2.163 0 4.118-.837 5.62-2.173l9.026 9.026c.098.098.226.147.354.147s.256-.049.354-.146c.195-.196.195-.512 0-.708zm-22.854-14.646c0-4.136 3.364-7.5 7.5-7.5s7.5 3.364 7.5 7.5-3.364 7.5-7.5 7.5-7.5-3.364-7.5-7.5z"/></svg>\
        </div>\
      </div>\
      <div id="s-Text_1" class="pie label singleline autofit firer ie-background commentable non-processed"   datasizewidth="319px" datasizeheight="21px" dataX="28" dataY="169" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Text_1_0">Join New Activities and Expand your Horizon</span></div></div></div></div>\
\
      <div id="s-Image_5" class="pie image firer click ie-background commentable non-processed"   datasizewidth="100px" datasizeheight="100px" dataX="28" dataY="226"   alt="image">\
          <img src="./images/2ba87155-5a71-4a1d-9798-dc85b0bf81a1.jpeg" />\
      </div>\
\
      <div id="s-Image_6" class="pie image firer ie-background commentable non-processed"   datasizewidth="100px" datasizeheight="100px" dataX="211" dataY="226"   alt="image">\
          <img src="./images/a2eb8f98-7e9d-49da-bb26-e29c7d9e54d5.jpg" />\
      </div>\
\
      <div id="s-Image_7" class="pie image firer ie-background commentable non-processed"   datasizewidth="100px" datasizeheight="100px" dataX="28" dataY="505"   alt="image">\
          <img src="./images/8a6631b0-fc46-40cb-9591-580b0b19e17a.jpg" />\
      </div>\
\
      <div id="s-Image_8" class="pie image firer ie-background commentable non-processed"   datasizewidth="100px" datasizeheight="100px" dataX="211" dataY="356"   alt="image">\
          <img src="./images/36602a72-e38f-4228-8e9e-7c98c2c2e9a4.jpg" />\
      </div>\
\
      <div id="s-Image_9" class="pie image firer click ie-background commentable non-processed"   datasizewidth="100px" datasizeheight="100px" dataX="28" dataY="356"   alt="image">\
          <img src="./images/d4451f77-fb20-40ab-b149-c05d495bc0ba.jpg" />\
      </div>\
\
      <div id="s-Image_10" class="pie image firer ie-background commentable non-processed"   datasizewidth="100px" datasizeheight="100px" dataX="211" dataY="505"   alt="image">\
          <img src="./images/1956694f-51e6-41a6-8bc9-7761440934ba.jpg" />\
      </div>\
      <div id="s-Table_1" class="pie percentage table firer ie-background commentable pin vpin-end hpin-beginning non-processed-percentage non-processed-pin non-processed"  datasizewidth="98%" datasizeheight="76px" dataX="0" dataY="10" originalwidth="367px" originalheight="76px" >\
        <div class="backgroundLayer"></div>\
        <table summary="">\
          <tbody>\
            <tr>\
              <td id="s-Cell_1" class="pie cellcontainer firer click ie-background non-processed"    datasizewidth="125px" datasizeheight="76px" dataX="0" dataY="0" originalwidth="125px" originalheight="76px" >\
                <div class="layout scrollable">\
                  <table class="layout" summary="">\
                    <tr>\
                      <td class="layout vertical insertionpoint verticalalign Cell_1 Table_1" valign="top" align="center" hSpacing="0" vSpacing="0"><div id="s-Image_1" class="pie image firer click ie-background commentable non-processed"   datasizewidth="24px" datasizeheight="24px" dataX="49" dataY="12"   alt="image" systemName="./images/b50c37a9-6395-44f5-a8b4-70bff1117c31.svg" overlay="#FC1268">\
                      <svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/></svg>\
                  </div></td> \
                    </tr>\
                  </table>\
\
                </div>\
              </td>\
              <td id="s-Cell_2" class="pie cellcontainer firer ie-background non-processed"    datasizewidth="117px" datasizeheight="76px" dataX="125" dataY="0" originalwidth="117px" originalheight="76px" >\
                <div class="layout scrollable">\
                  <table class="layout" summary="">\
                    <tr>\
                      <td class="layout vertical insertionpoint verticalalign Cell_2 Table_1" valign="top" align="center" hSpacing="0" vSpacing="0"><div id="s-Image_2" class="pie image firer ie-background commentable non-processed"   datasizewidth="24px" datasizeheight="24px" dataX="45" dataY="12"   alt="image" systemName="./images/65ffcdc0-b23f-4a58-8b54-831193b3ea38.svg" overlay="#FFFFFF">\
                      <svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M3 13h8V3H3v10zm0 8h8v-6H3v6zm10 0h8V11h-8v10zm0-18v6h8V3h-8z"/></svg>\
                  </div></td> \
                    </tr>\
                  </table>\
\
                </div>\
              </td>\
              <td id="s-Cell_3" class="pie cellcontainer firer click ie-background non-processed"    datasizewidth="125px" datasizeheight="76px" dataX="242" dataY="0" originalwidth="125px" originalheight="76px" >\
                <div class="layout scrollable">\
                  <table class="layout" summary="">\
                    <tr>\
                      <td class="layout vertical insertionpoint verticalalign Cell_3 Table_1" valign="top" align="center" hSpacing="0" vSpacing="0"><div id="s-Image_3" class="pie image firer click ie-background commentable non-processed"   datasizewidth="24px" datasizeheight="24px" dataX="49" dataY="12"   alt="image" systemName="./images/5a9b3257-02fa-4a07-bae9-64c251e47d27.svg" overlay="#FFFFFF">\
                      <svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/></svg>\
                  </div></td> \
                    </tr>\
                  </table>\
\
                </div>\
              </td>\
            </tr>\
          </tbody>\
        </table>\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;