(function(window, undefined) {

  var jimLinks = {
    "04c5c32f-20d2-474b-a99f-cd286945820b" : {
      "menuIcon" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "room" : [
        "1af0858b-5195-4a2b-80cd-8cc0f2d3084b"
      ]
    },
    "1af0858b-5195-4a2b-80cd-8cc0f2d3084b" : {
      "Image_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ]
    },
    "08cc01c8-380a-4b16-82f9-7fc21589ba74" : {
      "Panel_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "menuIcon" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "room" : [
        "a7d0ef2a-639a-4f55-992c-b04d85b07548"
      ],
      "Cell_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020",
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Image_1" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ]
    },
    "5f93d546-a288-47dd-a7b6-352050302db3" : {
      "Image_11" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Image_5" : [
        "22864188-0519-4dc3-8eec-06e1275b30b7"
      ],
      "Image_9" : [
        "08cc01c8-380a-4b16-82f9-7fc21589ba74"
      ],
      "Cell_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020",
        "4cf6703e-64a0-4e87-bd92-c36db8d79020",
        "4cf6703e-64a0-4e87-bd92-c36db8d79020",
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Image_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Cell_3" : [
        "b6c97015-ccdb-4250-af14-59c284dd8050"
      ],
      "Image_3" : [
        "b6c97015-ccdb-4250-af14-59c284dd8050"
      ]
    },
    "31f65544-0ce9-473b-b4af-4653bd7dba77" : {
      "Image_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ]
    },
    "d12245cc-1680-458d-89dd-4f0d7fb22724" : {
      "Image_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Cell_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Image_36" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Cell_5" : [
        "b6c97015-ccdb-4250-af14-59c284dd8050"
      ],
      "Image_76" : [
        "b6c97015-ccdb-4250-af14-59c284dd8050"
      ]
    },
    "a7d0ef2a-639a-4f55-992c-b04d85b07548" : {
      "Image_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ]
    },
    "a4ad34e3-1473-45d5-9be4-6901ad798673" : {
      "Image_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ]
    },
    "82d7bbbb-0454-4381-97ab-5aba19fd4244" : {
      "Cell_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Image_2" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Cell_3" : [
        "b6c97015-ccdb-4250-af14-59c284dd8050"
      ],
      "Image_6" : [
        "b6c97015-ccdb-4250-af14-59c284dd8050"
      ],
      "menuIcon" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Meditate" : [
        "31f65544-0ce9-473b-b4af-4653bd7dba77"
      ],
      "Name_1" : [
        "31f65544-0ce9-473b-b4af-4653bd7dba77"
      ]
    },
    "6c01d70c-e007-4ee2-a16a-9bc44f6a0ff9" : {
      "Cell_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020",
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Image_6" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Cell_3" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Image_8" : [
        "b6c97015-ccdb-4250-af14-59c284dd8050"
      ],
      "Image_9" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ]
    },
    "c716c12d-69f0-4863-8c15-f96c00faf0ff" : {
      "Button_2" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ]
    },
    "22864188-0519-4dc3-8eec-06e1275b30b7" : {
      "menuIcon" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "room" : [
        "a4ad34e3-1473-45d5-9be4-6901ad798673"
      ]
    },
    "b6c97015-ccdb-4250-af14-59c284dd8050" : {
      "Cell_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020",
        "4cf6703e-64a0-4e87-bd92-c36db8d79020",
        "4cf6703e-64a0-4e87-bd92-c36db8d79020",
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Image_1" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Image_3" : [
        "b6c97015-ccdb-4250-af14-59c284dd8050"
      ],
      "Image_9" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ]
    },
    "4cf6703e-64a0-4e87-bd92-c36db8d79020" : {
      "Image_11" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Image_15" : [
        "b6c97015-ccdb-4250-af14-59c284dd8050"
      ],
      "Button_1" : [
        "82d7bbbb-0454-4381-97ab-5aba19fd4244"
      ],
      "Button_3" : [
        "04c5c32f-20d2-474b-a99f-cd286945820b"
      ],
      "Image_1" : [
        "6c01d70c-e007-4ee2-a16a-9bc44f6a0ff9"
      ],
      "Image_16" : [
        "5f93d546-a288-47dd-a7b6-352050302db3"
      ],
      "Image_2" : [
        "4cf6703e-64a0-4e87-bd92-c36db8d79020"
      ],
      "Image_3" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);