(function(window, undefined) {

    /*********************** START STATIC ACCESS METHODS ************************/

    jQuery.extend(jimUtil, {
        "loadScrollBars": function() {
            jQuery(".s-1af0858b-5195-4a2b-80cd-8cc0f2d3084b .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:true, backgroundColor:'black', opacity:'0.5', thickness:'2'});
            jQuery(".s-1af0858b-5195-4a2b-80cd-8cc0f2d3084b #s-comments .layoutWrapper").overscroll({ showThumbs:false, direction:'vertical', roundCorners:true, backgroundColor:'black', opacity:'0.5', thickness:'2'});
            jQuery(".s-5f93d546-a288-47dd-a7b6-352050302db3 .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:true, backgroundColor:'black', opacity:'0.5', thickness:'2'});
            jQuery(".s-31f65544-0ce9-473b-b4af-4653bd7dba77 .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:true, backgroundColor:'black', opacity:'0.5', thickness:'2'});
            jQuery(".s-31f65544-0ce9-473b-b4af-4653bd7dba77 #s-comments .layoutWrapper").overscroll({ showThumbs:false, direction:'vertical', roundCorners:true, backgroundColor:'black', opacity:'0.5', thickness:'2'});
            jQuery(".s-a7d0ef2a-639a-4f55-992c-b04d85b07548 .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:true, backgroundColor:'black', opacity:'0.5', thickness:'2'});
            jQuery(".s-a7d0ef2a-639a-4f55-992c-b04d85b07548 #s-comments .layoutWrapper").overscroll({ showThumbs:false, direction:'vertical', roundCorners:true, backgroundColor:'black', opacity:'0.5', thickness:'2'});
            jQuery(".s-a4ad34e3-1473-45d5-9be4-6901ad798673 .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:true, backgroundColor:'black', opacity:'0.5', thickness:'2'});
            jQuery(".s-a4ad34e3-1473-45d5-9be4-6901ad798673 #s-comments .layoutWrapper").overscroll({ showThumbs:false, direction:'vertical', roundCorners:true, backgroundColor:'black', opacity:'0.5', thickness:'2'});
            jQuery(".s-6c01d70c-e007-4ee2-a16a-9bc44f6a0ff9 .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:true, backgroundColor:'black', opacity:'0.5', thickness:'2'});
            jQuery(".s-c716c12d-69f0-4863-8c15-f96c00faf0ff .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:true, backgroundColor:'black', opacity:'0.5', thickness:'2'});
            jQuery(".s-b6c97015-ccdb-4250-af14-59c284dd8050 .ui-page").overscroll({ showThumbs:true, direction:'vertical', roundCorners:true, backgroundColor:'black', opacity:'0.5', thickness:'2'});
         }
    });

    /*********************** END STATIC ACCESS METHODS ************************/

}) (window);