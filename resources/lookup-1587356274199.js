(function(window, undefined) {
  var dictionary = {
    "04c5c32f-20d2-474b-a99f-cd286945820b": "Skydiving",
    "1af0858b-5195-4a2b-80cd-8cc0f2d3084b": "SkydivingDetails",
    "08cc01c8-380a-4b16-82f9-7fc21589ba74": "Outdoors",
    "5f93d546-a288-47dd-a7b6-352050302db3": "Activities",
    "31f65544-0ce9-473b-b4af-4653bd7dba77": "YogaDetails",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "Profile",
    "a7d0ef2a-639a-4f55-992c-b04d85b07548": "HikingDetails",
    "a4ad34e3-1473-45d5-9be4-6901ad798673": "SurfingDetails",
    "82d7bbbb-0454-4381-97ab-5aba19fd4244": "YOGA",
    "6c01d70c-e007-4ee2-a16a-9bc44f6a0ff9": "Graphs",
    "c716c12d-69f0-4863-8c15-f96c00faf0ff": "Login",
    "22864188-0519-4dc3-8eec-06e1275b30b7": "Surfing",
    "b6c97015-ccdb-4250-af14-59c284dd8050": "chats",
    "4cf6703e-64a0-4e87-bd92-c36db8d79020": "Home-new",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);