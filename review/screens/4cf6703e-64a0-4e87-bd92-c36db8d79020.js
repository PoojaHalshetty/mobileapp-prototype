var content='<div class="ui-page" deviceName="iPhoneX" deviceType="mobile" deviceWidth="375" deviceHeight="812">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devMobile devIOS canvas firer commentable non-processed" alignment="left" name="Template 1" width="375" height="812">\
    <div id="backgroundBox"></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1587356274199.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1587356274199-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1587356274199-ie8.css" /><![endif]-->\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-4cf6703e-64a0-4e87-bd92-c36db8d79020" class="screen growth-none devMobile devIOS canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Home-new" width="375" height="812">\
    <div id="backgroundBox"></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/4cf6703e-64a0-4e87-bd92-c36db8d79020-1587356274199.css" />\
      <!--[if IE]><link type="text/css" rel="stylesheet" href="./resources/screens/4cf6703e-64a0-4e87-bd92-c36db8d79020-1587356274199-ie.css" /><![endif]-->\
      <!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="./resources/screens/4cf6703e-64a0-4e87-bd92-c36db8d79020-1587356274199-ie8.css" /><![endif]-->\
      <div id="s-Empty_Screen_Dark" class="group firer ie-background commentable non-processed" datasizewidth="375px" datasizeheight="812px" dataX="0" dataY="0" >\
        <div id="s-Rectangle_3" class="pie percentage rectangle firer commentable pin vpin-beginning hpin-beginning non-processed-percentage non-processed-pin non-processed"   datasizewidth="100%" datasizeheight="100%" dataX="0" dataY="0" >\
         <div class="backgroundLayer"></div>\
         <div class="paddingLayer">\
           <div class="clipping">\
             <div class="content">\
               <div class="valign">\
                 <span id="rtr-s-Rectangle_3_0"></span>\
               </div>\
             </div>\
           </div>\
         </div>\
        </div>\
        <div id="s-Rectangle_8" class="pie percentage rectangle firer commentable pin vpin-beginning hpin-beginning non-processed-percentage non-processed-pin non-processed"   datasizewidth="100%" datasizeheight="141px" dataX="0" dataY="0" >\
         <div class="backgroundLayer"></div>\
         <div class="paddingLayer">\
           <div class="clipping">\
             <div class="content">\
               <div class="valign">\
                 <span id="rtr-s-Rectangle_8_0"></span>\
               </div>\
             </div>\
           </div>\
         </div>\
        </div>\
        <div id="s-Rectangle_9" class="pie percentage rectangle firer commentable pin vpin-end hpin-beginning non-processed-percentage non-processed-pin non-processed"   datasizewidth="100%" datasizeheight="84px" dataX="0" dataY="0" >\
         <div class="backgroundLayer"></div>\
         <div class="paddingLayer">\
           <div class="clipping">\
             <div class="content">\
               <div class="valign">\
                 <span id="rtr-s-Rectangle_9_0"></span>\
               </div>\
             </div>\
           </div>\
         </div>\
        </div>\
        <div id="s-Home_Indicator_1" class="pie image firer ie-background commentable pin vpin-end hpin-center non-processed-pin non-processed"   datasizewidth="134px" datasizeheight="5px" dataX="0" dataY="12"   alt="image" systemName="./images/319deede-06e5-4a3e-ad98-555faf96eebb.svg" overlay="#FFFFFF">\
            <?xml version="1.0" encoding="UTF-8"?>\
            <svg preserveAspectRatio=\'none\' width="134px" height="5px" viewBox="0 0 134 5" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
                <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->\
                <title>Rectangle</title>\
                <desc>Created with Sketch.</desc>\
                <defs></defs>\
                <g id="UI-Bars-/-Home-Indicator-/-Home-Indicator---On-Light" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-121.000000, -20.000000)">\
                    <rect id="s-Home_Indicator_1-Rectangle" fill="#000000" x="121" y="20" width="134" height="5" rx="2.5"></rect>\
                </g>\
            </svg>\
        </div>\
        <div id="s-Text_2" class="pie label singleline firer pageload ie-background commentable pin vpin-beginning hpin-beginning non-processed-pin non-processed"   datasizewidth="45px" datasizeheight="19px" dataX="30" dataY="12" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Text_2_0">4:02</span></div></div></div></div>\
        <div id="s-Image_17" class="pie image firer ie-background commentable pin vpin-beginning hpin-end non-processed-pin non-processed"   datasizewidth="17px" datasizeheight="12px" dataX="63" dataY="12"   alt="image" systemName="./images/be9e187a-eff3-455e-b146-8779e52bc0cc.svg" overlay="#FFFFFF">\
            <?xml version="1.0" encoding="UTF-8"?>\
            <svg preserveAspectRatio=\'none\' width="18px" height="12px" viewBox="0 0 18 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
                <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->\
                <title>Mobile Signal</title>\
                <desc>Created with Sketch.</desc>\
                <defs></defs>\
                <g id="UI-Bars-/-Status-Bars-/-Black-Base" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-293.000000, -17.000000)">\
                    <path d="M294.666667,24.3333333 L295.666667,24.3333333 C296.218951,24.3333333 296.666667,24.7810486 296.666667,25.3333333 L296.666667,27.3333333 C296.666667,27.8856181 296.218951,28.3333333 295.666667,28.3333333 L294.666667,28.3333333 C294.114382,28.3333333 293.666667,27.8856181 293.666667,27.3333333 L293.666667,25.3333333 C293.666667,24.7810486 294.114382,24.3333333 294.666667,24.3333333 Z M299.333333,22.3333333 L300.333333,22.3333333 C300.885618,22.3333333 301.333333,22.7810486 301.333333,23.3333333 L301.333333,27.3333333 C301.333333,27.8856181 300.885618,28.3333333 300.333333,28.3333333 L299.333333,28.3333333 C298.781049,28.3333333 298.333333,27.8856181 298.333333,27.3333333 L298.333333,23.3333333 C298.333333,22.7810486 298.781049,22.3333333 299.333333,22.3333333 Z M304,20 L305,20 C305.552285,20 306,20.4477153 306,21 L306,27.3333333 C306,27.8856181 305.552285,28.3333333 305,28.3333333 L304,28.3333333 C303.447715,28.3333333 303,27.8856181 303,27.3333333 L303,21 C303,20.4477153 303.447715,20 304,20 Z M308.666667,17.6666667 L309.666667,17.6666667 C310.218951,17.6666667 310.666667,18.1143819 310.666667,18.6666667 L310.666667,27.3333333 C310.666667,27.8856181 310.218951,28.3333333 309.666667,28.3333333 L308.666667,28.3333333 C308.114382,28.3333333 307.666667,27.8856181 307.666667,27.3333333 L307.666667,18.6666667 C307.666667,18.1143819 308.114382,17.6666667 308.666667,17.6666667 Z" id="s-Image_17-Mobile-Signal" fill="#000000" fill-rule="nonzero"></path>\
                </g>\
            </svg>\
        </div>\
        <div id="s-Image_18" class="pie image firer ie-background commentable pin vpin-beginning hpin-end non-processed-pin non-processed"   datasizewidth="18px" datasizeheight="18px" dataX="40" dataY="9"   alt="image" systemName="./images/33787012-3ccf-4060-9a44-ccc012c52817.svg" overlay="#FFFFFF">\
            <svg preserveAspectRatio=\'none\' id="s-Image_18-Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>cacaca</title><path d="M12,8.06a12.51,12.51,0,0,1,8.27,3.12L21.8,9.46A15,15,0,0,0,12,5.54,15,15,0,0,0,2.2,9.45l1.53,1.72A12.49,12.49,0,0,1,12,8.06"/><path d="M12,13a7.6,7.6,0,0,1,5,1.85l1.63-1.82A10.07,10.07,0,0,0,12,10.5,10.08,10.08,0,0,0,5.4,13L7,14.87A7.61,7.61,0,0,1,12,13"/><path d="M15.34,16.69A5.24,5.24,0,0,0,12,15.4a5.24,5.24,0,0,0-3.34,1.29L12,20.44Z"/></svg>\
        </div>\
        <div id="s-Image_4" class="pie image firer ie-background commentable pin vpin-beginning hpin-end non-processed-pin non-processed"   datasizewidth="25px" datasizeheight="12px" dataX="10" dataY="14"   alt="image" systemName="./images/d9f11b9b-7021-4213-91a0-99b6c9963b6b.svg" overlay="#FFFFFF">\
            <?xml version="1.0" encoding="UTF-8"?>\
            <svg preserveAspectRatio=\'none\' width="25px" height="12px" viewBox="0 0 25 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
                <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->\
                <title>Battery</title>\
                <desc>Created with Sketch.</desc>\
                <defs></defs>\
                <g id="Bars/Status-Bar/Dark-Status-Bar" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-336.000000, -17.000000)">\
                    <g id="s-Image_4-Battery" transform="translate(336.000000, 17.000000)">\
                        <rect id="s-Image_4-Border" stroke="#FFFFFF" opacity="0.35" x="0.5" y="0.833333333" width="21" height="10.3333333" rx="2.66666675"></rect>\
                        <path d="M23,4 L23,8 C23.8047311,7.66122348 24.328038,6.87313328 24.328038,6 C24.328038,5.12686672 23.8047311,4.33877652 23,4" id="s-Image_4-Cap" fill="#FFFFFF" fill-rule="nonzero" opacity="0.4"></path>\
                        <rect id="s-Image_4-Capacity" fill="#FFFFFF" fill-rule="nonzero" x="2" y="2.33333333" width="18" height="7.33333333" rx="1.33333337"></rect>\
                    </g>\
                </g>\
            </svg>\
        </div>\
        <div id="s-Table_1" class="pie percentage table firer ie-background commentable pin vpin-end hpin-beginning non-processed-percentage non-processed-pin non-processed"  datasizewidth="100%" datasizeheight="84px" dataX="0" dataY="0" originalwidth="375px" originalheight="84px" >\
          <div class="backgroundLayer"></div>\
          <table summary="">\
            <tbody>\
              <tr>\
                <td id="s-Cell_1" class="pie cellcontainer firer ie-background non-processed"    datasizewidth="125px" datasizeheight="84px" dataX="0" dataY="0" originalwidth="125px" originalheight="84px" >\
                  <div class="layout scrollable">\
                    <table class="layout" summary="">\
                      <tr>\
                        <td class="layout vertical insertionpoint verticalalign Cell_1 Table_1" valign="top" align="center" hSpacing="0" vSpacing="0"><div id="s-Image_11" class="pie image firer click ie-background commentable non-processed"   datasizewidth="24px" datasizeheight="24px" dataX="49" dataY="12"   alt="image" systemName="./images/b50c37a9-6395-44f5-a8b4-70bff1117c31.svg" overlay="#FC1268">\
                        <svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/></svg>\
                    </div></td> \
                      </tr>\
                    </table>\
\
                  </div>\
                </td>\
                <td id="s-Cell_3" class="pie cellcontainer firer ie-background non-processed"    datasizewidth="125px" datasizeheight="84px" dataX="125" dataY="0" originalwidth="125px" originalheight="84px" >\
                  <div class="layout scrollable">\
                    <table class="layout" summary="">\
                      <tr>\
                        <td class="layout vertical insertionpoint verticalalign Cell_3 Table_1" valign="top" align="center" hSpacing="0" vSpacing="0"><div id="s-Image_13" class="pie image firer ie-background commentable non-processed"   datasizewidth="24px" datasizeheight="24px" dataX="49" dataY="12"   alt="image" systemName="./images/65ffcdc0-b23f-4a58-8b54-831193b3ea38.svg" overlay="#FFFFFF">\
                        <svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M3 13h8V3H3v10zm0 8h8v-6H3v6zm10 0h8V11h-8v10zm0-18v6h8V3h-8z"/></svg>\
                    </div></td> \
                      </tr>\
                    </table>\
\
                  </div>\
                </td>\
                <td id="s-Cell_5" class="pie cellcontainer firer ie-background non-processed"    datasizewidth="125px" datasizeheight="84px" dataX="250" dataY="0" originalwidth="125px" originalheight="84px" >\
                  <div class="layout scrollable">\
                    <table class="layout" summary="">\
                      <tr>\
                        <td class="layout vertical insertionpoint verticalalign Cell_5 Table_1" valign="top" align="center" hSpacing="0" vSpacing="0"><div id="s-Image_15" class="pie image firer click ie-background commentable non-processed"   datasizewidth="24px" datasizeheight="24px" dataX="49" dataY="12"   alt="image" systemName="./images/5a9b3257-02fa-4a07-bae9-64c251e47d27.svg" overlay="#FFFFFF">\
                        <svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/></svg>\
                    </div></td> \
                      </tr>\
                    </table>\
\
                  </div>\
                </td>\
              </tr>\
            </tbody>\
          </table>\
        </div>\
      </div>\
      <div id="s-Home_Indicator" class="pie image firer ie-background commentable pin vpin-end hpin-center non-processed-pin non-processed"   datasizewidth="134px" datasizeheight="5px" dataX="0" dataY="12"   alt="image" systemName="./images/4e316227-ec68-48a6-b16a-bc508a0477f6.svg" overlay="#000000">\
          <?xml version="1.0" encoding="UTF-8"?>\
          <svg preserveAspectRatio=\'none\' width="134px" height="5px" viewBox="0 0 134 5" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
              <!-- Generator: Sketch 48.2 (47327) - http://www.bohemiancoding.com/sketch -->\
              <title>Rectangle</title>\
              <desc>Created with Sketch.</desc>\
              <defs></defs>\
              <g id="UI-Bars-/-Home-Indicator-/-Home-Indicator---On-Light" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-121.000000, -20.000000)">\
                  <rect id="s-Home_Indicator-Rectangle" fill="#000000" x="121" y="20" width="134" height="5" rx="2.5"></rect>\
              </g>\
          </svg>\
      </div>\
      <div id="s-Carousel" class="pie percentage dynamicpanel firer ie-background commentable non-processed-percentage non-processed" datasizewidth="99%" datasizeheight="540px" dataX="0" dataY="155" >\
        <div id="s-items" class="pie percentage panel default firer ie-background commentable non-processed-percentage non-processed"  datasizewidth="99%" datasizeheight="540px" >\
          <div class="backgroundLayer"></div>\
          <div class="layoutWrapper scrollable">\
              <div id="s-adventures" class="pie table firer ie-background commentable non-processed"  datasizewidth="330px" datasizeheight="540px" dataX="21" dataY="0" originalwidth="330px" originalheight="540px" >\
                <div class="backgroundLayer"></div>\
                <table summary="">\
                  <tbody>\
                    <tr>\
                      <td id="s-Cell_8" class="pie cellcontainer firer ie-background non-processed"  colspan="2"  datasizewidth="330px" datasizeheight="257px" dataX="0" dataY="0" originalwidth="330px" originalheight="257px" >\
                        <div class="layout scrollable">\
                          <table class="layout" summary="">\
                            <tr>\
                              <td class="layout vertical insertionpoint verticalalign Cell_8 adventures" valign="top" align="left" hSpacing="0" vSpacing="0"><div id="s-Text_1" class="pie label singleline autofit firer ie-background commentable non-processed"   datasizewidth="10px" datasizeheight="18px" dataX="0" dataY="0" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Text_1_0"></span></div></div></div></div>\
                          <div id="s-Image_7" class="pie image firer ie-background commentable non-processed"   datasizewidth="333px" datasizeheight="235px" dataX="0" dataY="18"   alt="image">\
                              <img src="./images/b79b0070-42fb-495b-b67a-e3e519ed46cc.jpg" />\
                          </div></td> \
                            </tr>\
                          </table>\
\
                        </div>\
                      </td>\
                    </tr>\
                    <tr>\
                      <td id="s-Cell_4" class="pie cellcontainer firer ie-background non-processed"    datasizewidth="163px" datasizeheight="283px" dataX="0" dataY="257" originalwidth="163px" originalheight="283px" >\
                        <div class="layout scrollable">\
                          <table class="layout" summary="">\
                            <tr>\
                              <td class="layout vertical insertionpoint verticalalign Cell_4 adventures" valign="top" align="left" hSpacing="0" vSpacing="0"><div id="s-Text_3" class="pie label singleline autofit firer ie-background commentable non-processed"   datasizewidth="10px" datasizeheight="28px" dataX="0" dataY="0" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Text_3_0"></span></div></div></div></div><div id="s-Button_5" class="pie button singleline firer ie-background commentable non-processed"   datasizewidth="162px" datasizeheight="47px" dataX="0" dataY="28" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Button_5_0"> &nbsp; My Activities</span></div></div></div></div><div id="s-Button_1" class="pie button singleline firer click ie-background commentable non-processed"   datasizewidth="162px" datasizeheight="46px" dataX="0" dataY="75" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Button_1_0">YOGA</span></div></div></div></div><div id="s-Button_3" class="pie button singleline firer click ie-background commentable non-processed"   datasizewidth="162px" datasizeheight="40px" dataX="0" dataY="121" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Button_3_0">OUTDOOR</span></div></div></div></div><div id="s-Button_2" class="pie button singleline firer ie-background commentable non-processed"   datasizewidth="162px" datasizeheight="55px" dataX="0" dataY="161" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Button_2_0">MUSIC</span></div></div></div></div><div id="s-Button_4" class="pie button singleline firer ie-background commentable non-processed"   datasizewidth="162px" datasizeheight="49px" dataX="0" dataY="216" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Button_4_0">STUDY</span></div></div></div></div></td> \
                            </tr>\
                          </table>\
\
                        </div>\
                      </td>\
                      <td id="s-Cell_6" class="pie cellcontainer firer ie-background non-processed"    datasizewidth="167px" datasizeheight="283px" dataX="163" dataY="257" originalwidth="167px" originalheight="283px" >\
                        <div class="layout scrollable">\
                          <table class="layout" summary="">\
                            <tr>\
                              <td class="layout vertical insertionpoint verticalalign Cell_6 adventures" valign="top" align="left" hSpacing="0" vSpacing="0">\
                          <div id="s-Image_1" class="pie image firer click ie-background commentable non-processed"  title="Statistics" datasizewidth="170px" datasizeheight="128px" dataX="0" dataY="0"   alt="image">\
                              <img src="./images/9343b237-837b-4a23-af6f-dacd7cadd414.jpg" />\
                          </div><div id="s-Text_4" class="pie label singleline autofit firer ie-background commentable non-processed"   datasizewidth="114px" datasizeheight="18px" dataX="0" dataY="128" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Text_4_0"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Statistics</span></div></div></div></div>\
                          <div id="s-Image_16" class="pie image firer click ie-background commentable non-processed"   datasizewidth="170px" datasizeheight="104px" dataX="0" dataY="146"   alt="image">\
                              <img src="./images/93b8ff45-27af-4665-bebc-884a92acadbb.jpg" />\
                          </div><div id="s-Text_5" class="pie label singleline autofit firer ie-background commentable non-processed"   datasizewidth="145px" datasizeheight="18px" dataX="0" dataY="250" ><div class="backgroundLayer"></div><div class="paddingLayer"><div class="content"><div class="valign"><span id="rtr-s-Text_5_0"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span id="rtr-s-Text_5_1">Add New Activities</span></div></div></div></div></td> \
                            </tr>\
                          </table>\
\
                        </div>\
                      </td>\
                    </tr>\
                  </tbody>\
                </table>\
              </div>\
\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Image_2" class="pie image firer click ie-background commentable non-processed"   datasizewidth="48px" datasizeheight="45px" dataX="23" dataY="66"   alt="image">\
          <img src="./images/be0d4741-4afe-4a1a-af0f-0cb8433ddf73.png" />\
      </div>\
\
      <div id="s-Image_3" class="pie image firer click ie-background commentable non-processed"   datasizewidth="37px" datasizeheight="34px" dataX="313" dataY="72"   alt="image">\
          <img src="./images/06e45809-adad-4668-b430-b91cdb35b972.jpg" />\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;